<?php
    $khoa = array('' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'khoa học vật liệu');
?>
<script>
window.onload = function() {
    
    var khoa = localStorage.getItem("khoa");
    var keyWord = localStorage.getItem("keyWord");
    const collection = document.getElementsByClassName("input");
    collection[0].value = khoa;
    collection[1].value = keyWord;
}

function deleteValue(a) {
    const collection = document.getElementsByClassName(a);
    for (let i = 0; i < collection.length; i++) {
        collection[i].value = "";
    }
    localStorage.removeItem("khoa");
    localStorage.removeItem("keyWord");
}

function saveValue(a) {
    const collection = document.getElementsByClassName(a);
    localStorage.setItem("khoa",collection[0].value);
    localStorage.setItem("keyWord",collection[1].value);
    console.log(localStorage.getItem(khoa));
}
</script>
<html>
<head>
    <title>Trang đăng nhập</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="ex7.css">
</head>

<body>
    <div class="mx-auto mt-5 border d-flex flex-column" style="max-width: 700px; border: 2px solid #2980d7 !important">
        <div class="my-3 d-flex flex-column mx-auto">
                <div class="searchDiv">
                    <label class="label">Khoa</label>
                    <div class="fillInfo">
                        <select id="khoa" class="inp input" name="department" style="width: 189px; height:100%; border: 1.5px solid #58769a; background:#e1eaf4
">
                            <?php foreach ($khoa as $i) : ?>
                            <option><?php echo $i ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="d-flex mb-2" style="height: 35px">
                    <label class="input-label h-100 mr-3">Từ khóa</label>
                    <div class="fillInfo">
                        <input id="keyWord" class="h-100 input" type="text" style="width: 189px; height:100%; border: 1.5px solid #58769a;background:#e1eaf4" name="key">
                    </div>
                </div>
                <div class="d-flex">
                    <button onclick="deleteValue('input')" class="btn1 mx-auto btn btn-primary btn-submit mt-3">Xóa</button>
                    <button onclick="saveValue('input')" class="btn1 mx-auto btn btn-primary btn-submit mt-3">Tìm kiếm</button>
                </div>
        </div>
        <div style="display:flex;margin-right:10px;margin-left:10px">
            <span style="font-weight: 500">Số sinh viên tìm thấy : xxx</span>
            <div style="margin-left:auto; margin-right:40px">
                <a href="regist_form.php" class="btn1 btn btn-primary btn-add">Thêm</a>
            </div>
        </div>
        <div class="mt-3">
            <table>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td>Lê Hồng Phương</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <div>
                            <button class="btn2 btn btn-action">Xóa</button>
                            <button class="btn2 btn btn-action">Sửa</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Đỗ Thanh Hà</td>
                    <td>Khoa học máy tính</td>
                    <td>
                        <div>
                            <button class="btn2 btn btn-action">Xóa</button>
                            <button class="btn2 btn btn-action">Sửa</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Ngô Thế Quyền</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <div>
                            <button class="btn2 btn btn-action">Xóa</button>
                            <button class="btn2 btn btn-action">Sửa</button>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Nguyễn Hải Vinh</td>
                    <td>Khoa học vật liệu</td>
                    <td>
                        <div>
                            <button class="btn2 btn btn-action">Xóa</button>
                            <button class="btn2 btn btn-action">Sửa</button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>
